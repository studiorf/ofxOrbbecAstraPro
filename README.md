# ofxOrbbecAsPro

Simple wrapper for the Orbbec Astra camera series in Linux ARM x86 architecture only.<br>
Please check the branch `ARM` for more information.

# Dependencies

- Check [Orbbec Develop](https://orbbec3d.com/develop/) for [Astra SDK 2.1.0](http://dl.orbbec3d.com/dist/astra/v2.1.0/AstraSDK-v2.1.0-9bced77c41-Linux-arm.zip)
- Open Frameworks v0.11.0 or higher

# Hardware results

Tested only on Raspberry Pi 4 Model B with 2GB RAM.